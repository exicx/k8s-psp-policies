# Testing OPA / Gatekeeper / Pod Security Policy framework

https://kubernetes.io/docs/concepts/policy/pod-security-policy/

https://kubernetes.io/docs/concepts/security/pod-security-standards/#policy-types


# Baseline security policy (in Deny mode)

1. try to use host namespace
```
    spec.hostNetwork
    spec.hostPID
    spec.hostIPC
    spec.containers[*].ports[*].hostPort
    spec.initContainers[*].ports[*].hostPort
```
1. try to start privileged container
```
    spec.containers[*].securityContext.privileged
    spec.initContainers[*].securityContext.privileged
```
1. try to mount host volume
```
    spec.volumes[*].hostPath
```
1. try to add additional Linux capabilities beyond default set
```
    spec.containers[*].securityContext.capabilities.add
    spec.initContainers[*].securityContext.capabilities.add
```
1. try to change /proc mount options
```
    spec.containers[*].securityContext.procMount
    spec.initContainers[*].securityContext.procMount
```
1. try to set sysctls on host
```
    spec.securityContext.sysctls
```
1. (optional) try to set custom SELinux options
```
    spec.securityContext.seLinuxOptions
    spec.containers[*].securityContext.seLinuxOptions
    spec.initContainers[*].securityContext.seLinuxOptions
```
1. (optional) try to set custom AppArmor options
```
    metadata.annotations['container.apparmor.security.beta.kubernetes.io/*']
```
# Restricted security policy (in Audit mode)

1. everything in the baseline policy above
1. try to mount invalid volume types
```
    spec.volumes[*].hostPath
    spec.volumes[*].gcePersistentDisk
    spec.volumes[*].awsElasticBlockStore
    spec.volumes[*].gitRepo
    spec.volumes[*].nfs
    spec.volumes[*].iscsi
    spec.volumes[*].glusterfs
    spec.volumes[*].rbd
    spec.volumes[*].flexVolume
    spec.volumes[*].cinder
    spec.volumes[*].cephFS
    spec.volumes[*].flocker
    spec.volumes[*].fc
    spec.volumes[*].azureFile
    spec.volumes[*].vsphereVolume
    spec.volumes[*].quobyte
    spec.volumes[*].azureDisk
    spec.volumes[*].portworxVolume
    spec.volumes[*].scaleIO
    spec.volumes[*].storageos
    spec.volumes[*].csi
```
1. try to run privilege escalation (set uid, set gid)
```
    spec.containers[*].securityContext.allowPrivilegeEscalation
    spec.initContainers[*].securityContext.allowPrivilegeEscalation
```
1. try to run container as root user
```
    spec.securityContext.runAsNonRoot
    spec.containers[*].securityContext.runAsNonRoot
    spec.initContainers[*].securityContext.runAsNonRoot
```
1. try to run container as root group
````
    spec.securityContext.runAsGroup
    spec.securityContext.supplementalGroups[*]
    spec.securityContext.fsGroup
    spec.containers[*].securityContext.runAsGroup
    spec.initContainers[*].securityContext.runAsGroup
````
1. try to apply an invalid seccomp profile
```
    spec.securityContext.seccompProfile.type
    spec.containers[*].securityContext.seccompProfile
    spec.initContainers[*].securityContext.seccompProfile
```
